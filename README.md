### Overview

For the purposes of our NLP solution, we are processing [Wikidata](https://www.wikidata.org/wiki/Q91) into items stored in our Knowledgebase. Wikidata are in RDF format which can be stored in files with ".nt" extension.

In the file `wikidata_ids.txt` there are 100 wikidata ids and in the file `100_items_wikidata_subset.nt` there is a subset of wikidata for these ids. Your task is to write a Python program which will process these ids and output JSONs such as the one in file `KB-91-W.json` - this is our serialized Knowledgebase item. Dont worry if you only finish part of the assignment.


Overview of JSON attributes (where applicable, we are interested only in lang `cs` and `en`):
* kbid = id of the item
	* `Q91` translates to `KB-91-W`
* hrid = kbid together with the english stdform
* stdforms: 
	* taken from predicate http://www.w3.org/2000/01/rdf-schema#label
* labels = used to search for in text
	* taken from predicates http://www.w3.org/2000/01/rdf-schema#label, http://www.w3.org/2004/02/skos/core#altLabel, http://schema.org/alternateName
	* an item's label may be "turned off" by our convention of adding an "stability=-10" to it (see label "Lincoln" in `KB-91-W.json` example)
* descriptions = short text as description
	* taken from predicate http://schema.org/description in Wikidata
* ids = external ids in wikidata
	* should contain `wikidata` and `freebase`
* rankings = in `wikilinks` field we have the number of references in wikidata
* type: 
	* taken from the the class hierarchy in wikidata
	* predicates http://www.wikidata.org/prop/direct/P279 and http://www.wikidata.org/prop/direct/P31

### Tasks

1. Write a program that processes `100_items_wikidata_subset.nt` and outputs JSONs with: kbid, hrid, stdforms, labels, descriptions, ids and rankings.
2. For disambiguation reasons, we do not want to use one word labels. Set the `stability` of all the one word labels to -10, as it is in the `KB-91-W.json` example.
3. Find a way to use the class hierarchy in Wikidata to also set the type in one of the following: `['organization', persons', location', product', event', general']`. It does not have to be perfect, try to find a translation of classes in WD to one of these 6 types.

Bonus points are if you create a `KbItem` class and not work directly with JSON.

### Tips

Tips that might come in handy (though it is not obligatory to use them):
* there is a `rdflib` Python library for working with RDF triples: https://rdflib.readthedocs.io/en/stable/gettingstarted.html
* wikidata can be queried through SPARQL here: https://query.wikidata.org/
* here is a useful SPARQL tutorial: https://www.wikidata.org/wiki/Wikidata:SPARQL_tutorial
* `rdflib.Graph.query()` can be used to query the graph using SPARQL