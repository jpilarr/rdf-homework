import json
import pprint
import requests
from rdflib import Graph, query
from rdflib.term import URIRef




class RdfData(object):
        
    def get_kbid(self):
        return self.kbid
    
    def set_kbid(self, value):
        """Set kdbid.

        :param value: Q91 translates to KB-91-W 
        :type value: str
        """        
        value_slice = f"KB-{value[1:]}-W"
        self.kbid = value_slice
    
    def get_hrid(self):
        return self.hrid
    
    def set_hrid(self, value):
        self.hrid = value
    
    def get_stdforms(self):
        return self.stdforms
    
    def set_stdforms(self, value):
        self.stdforms = value
        
    def get_labels(self):
        return self.labels
    
    def set_labels(self, label_en, label_cs):
        def set_value(label, lang):
            lang_dict = getattr(self, lang)
            for _, lang_item in label:
                if lang_item not in lang_dict: 
                    lang_dict[lang_item.toPython()] = dict(
                        text=lang_item.toPython())
        
        self.cs = {}
        self.en = {}
        set_value(label_en, 'en')
        set_value(label_cs, 'cs')
        
        self.labels = {"cs": self.cs, "en": self.en}
        del self.cs
        del self.en
        
        
    def get_descriptions(self):
        return self.descriptions
    
    def set_descriptions(self, value):
        self.descriptions = value
        
    def get_ids(self):
        return self.ids
    
    def set_ids(self, value):
        self.ids = value
    
        
    def get_rankings(self):
        return self.rankings
    
    def set_rankings(self, value):
        self.rankings = value
    
    
        
        

class RdfSerialize:
    
    def __init__(self) -> None:
        self.graph = None
    
    def load_graph(self, path):
        self.graph = Graph()
        self.graph.parse(path, format="nt")
        self.graph
        pprint.pprint(len(self.graph))
        
        # for subj, pred, obj in self.graph:
        #     print(f'sub>{subj} obj>{obj} pred> {pred}')

    def count_wikilinks(self, rdf_id):
        query = """SELECT (COUNT(*) AS ?count) 
            WHERE { ?item wd:Q58494516 wd:"""+str(rdf_id)+" }"
         
        response = self.graph.query(query)
        print(f' res>> {response[0]}')
        
        
def main():
        
    rdf = RdfSerialize()
    rdf.load_graph("100_items_wikidata_subset.nt")

    URL_BASE = 'http://www.wikidata.org/entity/'


    with open('wikidata_ids.txt') as rdf_ids_file:
   
        for line in rdf_ids_file.readlines():
            rdf_id = line.strip()
            url = URL_BASE + rdf_id
            subj = URIRef(url)
            data = RdfData()
            for p, o in rdf.graph.predicate_objects(subject=subj):
                
                label_en = rdf.graph.preferredLabel(subj, lang='en')
                label_cs = rdf.graph.preferredLabel(subj, lang='cs')
                data.set_kbid(rdf_id)
                data.set_labels(label_en,label_cs)
                # data.set_rankings(rdf.count_wikilinks(rdf_id)) 
                print(f'url>{url} predi> {p}, obj> {o} label en>{label_en} label cs>{label_cs}')
                
            json_file = open(f'{data.get_kbid()}.json', 'w')  
            json_file.write(json.dumps(data, default=vars))
                
if __name__ == '__main__':
    main()
